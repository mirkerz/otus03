"""
Set of fields used as building block to make and validate incoming data scheme
"""
import datetime

from .constants import GENDERS
from .exceptions import ValidationError


class Field(object):
    """ Processor & validator of external data """

    # Value that should be used as null value
    empty_value = None
    # Values that will be considered as null
    empty_values = [None]

    def __init__(self, *, required=True, nullable=False):
        self.nullable = nullable
        self.required = required

    def validate(self, value):
        """ Verify that given python representation of object is fit to business requirements """
        if not self.nullable and value in self.empty_values:
            raise ValidationError("Can't be empty")

    def to_python(self, raw_value):
        """ Process initial value into appropriate python object """
        if raw_value in self.empty_values:
            return self.empty_value
        return raw_value

    def clean(self, raw_value):
        """ Process initial value, cast it into python object, store into value
        attribute and validate  that this object is useful """
        value = self.to_python(raw_value)
        self.validate(value)
        self.value = value
        return value

    def __eq__(self, value):
        """ Naive comparison with other python object by proxy it to value instance """
        return self.value == value


class CharField(Field):
    """ Text data """

    empty_values = [None, ""]
    empty_value = ""

    def validate(self, value):
        super().validate(value)
        if value not in self.empty_values and not isinstance(value, str):
            raise ValidationError("Value must be a string")

    def process(raw_value):
        value = super().process(raw_value)
        return str(value)


class ChoicesField(Field):
    """ Data from given set of values. Adds choices as initial parameter, store
    it as instance attribute """

    def __init__(self, *, choices, **kwargs):
        self.choices = choices
        super().__init__(**kwargs)

    def validate(self, value):
        super().validate(value)
        if value and value not in self.choices:
            raise ValidationError("Invalid choice")

    def clean(self, raw_value):
        value = super().clean(raw_value)
        self.choice = value
        self.value = self.choices[value]
        return self.value


class ArgumentsField(Field):
    """ Arbitrary dictionary. Schema validation could be added if needed """

    def to_python(self, raw_value):
        value = raw_value
        if not isinstance(value, dict):
            raise ValidationError("Json should represent dictionary")
        return value


class EmailField(CharField):
    """ Char field with @ in it """

    def validate(self, value):
        super().validate(value)
        if "@" not in value:
            raise ValidationError("Wrong email format")


class PhoneField(Field):
    """ String or number of length 11 and first symbol/number 7 """

    def validate(self, value):
        super().validate(value)
        if not (isinstance(value, (int, str))):
            raise ValidationError("Invalid value type")
        value = str(value)
        if len(value) != 11 or value[0] != "7":
            raise ValidationError("Wrong phone format")


class DateField(Field):
    """ Date in DD.MM.YYYY format """

    def to_python(self, raw_value):
        value = super().to_python(raw_value)
        try:
            date = datetime.datetime.strptime(value, "%d.%m.%Y")
        except ValueError:
            raise ValidationError("Wrong date format")
        return date


class BirthDayField(DateField):
    """ Date not older that ~70 years """

    def validate(self, value):
        super().validate(value)
        if not value:
            return
        # 70 / 4 = 17.5 shamefully ignore leap years
        if datetime.datetime.now() - value > datetime.timedelta(days=70 * 365):
            raise ValidationError("Person is too old")


class GenderField(ChoicesField):
    def __init__(self, **kwargs):
        super().__init__(choices=GENDERS, **kwargs)


class ClientIDsField(Field):
    """ List of integer values """

    empty_values = [[]]
    empty_value = []

    def validate(self, value):
        super().validate(value)
        if not isinstance(value, list):
            raise ValidationError("Not a list")
        if not all((isinstance(client_id, int) for client_id in value)):
            raise ValidationError("client_id should be a number")
