import unittest

from ..exceptions import ValidationError
from .. import fields


class TestSuite(unittest.TestCase):
    def test_field_nullable(self):
        f1 = fields.Field(nullable=True)
        self.assertEqual(f1.clean(None), None)
        f2 = fields.Field(nullable=False)
        self.assertRaises(ValidationError, f2.clean, None)

    def test_char_field(self):
        f = fields.CharField()
        self.assertRaises(ValidationError, f.clean, 42)

    def test_choices_field(self):
        choices = {42: "42"}
        f = fields.ChoicesField(choices=choices)
        for choice in choices:
            self.assertEqual(f.clean(choice), choices[choice])
        self.assertRaises(ValidationError, f.clean, 6)

    def test_arguments_field(self):
        f = fields.ArgumentsField()
        self.assertEqual(f.clean({42: 42}), {42: 42})
        self.assertRaises(ValidationError, f.clean, 42)

    def test_email_field(self):
        f = fields.EmailField()
        self.assertEqual(f.clean("foo@bar"), "foo@bar")
        self.assertRaises(ValidationError, f.clean, "foobar")

    def test_phone_field(self):
        f = fields.PhoneField()
        self.assertEqual(f.clean(71111111111), 71111111111)
        self.assertEqual(f.clean("71111111111"), "71111111111")
        self.assertRaises(ValidationError, f.clean, "7")
        self.assertRaises(ValidationError, f.clean, "11111111111")

    def test_date_field(self):
        f = fields.DateField()
        import datetime

        self.assertEqual(
            f.clean("01.10.1999"),
            datetime.datetime(day=1, month=10, year=1999),
        )

    def test_birthday_field(self):
        f = fields.BirthDayField()
        import datetime

        self.assertRaises(
            ValidationError,
            f.clean,
            (
                datetime.datetime.now() - datetime.timedelta(days=71 * 365)
            ).strftime("%d.%m.%Y"),
        )

    def test_client_ids_field(self):
        f = fields.ClientIDsField()
        self.assertEqual(f.clean([1, 2, 3]), [1, 2, 3])
        self.assertRaises(ValidationError, f.clean, (1, 2, 3))


if __name__ == "__main__":
    unittest.main()
