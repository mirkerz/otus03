#!/usr/bin/env python
import json
import uuid
import hashlib
import logging
import datetime

from optparse import OptionParser
from http.server import BaseHTTPRequestHandler, HTTPServer

from .requests import MethodRequest
from .handlers import HANDLERS
from .constants import (
    ADMIN_SALT,
    BAD_REQUEST,
    INVALID_REQUEST,
    ERRORS,
    INTERNAL_ERROR,
    FORBIDDEN,
    NOT_FOUND,
    OK,
    SALT,
)


def check_auth(request):
    """
    Проверка аутентификационных данных в запросе
    """
    if request.is_admin:
        digest = hashlib.sha512(
            (datetime.datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT).encode(
                "utf-8"
            )
        ).hexdigest()
    else:
        digest = hashlib.sha512(
            (request.account.value + request.login.value + SALT).encode(
                "utf-8"
            )
        ).hexdigest()
    if digest == request.token.value:
        return True
    return False


def method_handler(request, ctx, store):
    """
    Обработчик структурированных запросов.  Запросы состоят из общих
    параметров, метода и аргументов метода.  Обработчик проверяет базовую
    структуру запроса, аутентификауию и вызывает запрашиваемый метод с
    запрашиваемыми параметрами

    request :: словарь данных запроса
    ctx :: контекст/глобальный сторедж для хранения
        дополнительных данных полученных при обработке запроса
    store :: id необходимый для работы конечных методов бекенда
    """
    method_request = MethodRequest(request["body"])
    if not method_request.is_valid():
        return method_request.error_messages, INVALID_REQUEST
    if not check_auth(method_request):
        return "Forbidden", FORBIDDEN
    arguments = method_request.clean_data["arguments"]
    method = method_request.clean_data["method"](arguments)
    if not method.is_valid():
        return method.error_messages, INVALID_REQUEST
    handler = HANDLERS[method_request.method.choice]
    response = handler(
        store=store,
        is_admin=method_request.is_admin,
        data=method.clean_data
    )
    ctx.update(method.get_context())
    return response, OK


class MainHTTPHandler(BaseHTTPRequestHandler):
    """ Хендлер запросов к питоновскому http серверу """

    router = {"method": method_handler}
    store = None

    def get_request_id(self, headers):
        return headers.get("HTTP_X_REQUEST_ID", uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers["Content-Length"]))
            request = json.loads(data_string)
        except Exception:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info(
                "%s: %s %s" % (self.path, data_string, context["request_id"])
            )
            if path in self.router:
                try:
                    response, code = self.router[path](
                        {"body": request, "headers": self.headers},
                        context,
                        self.store,
                    )
                except Exception as e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {
                "error": response or ERRORS.get(code, "Unknown Error"),
                "code": code,
            }
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r).encode("utf-8"))
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(
        filename=opts.log,
        level=logging.INFO,
        format="[%(asctime)s] %(levelname).1s %(message)s",
        datefmt="%Y.%m.%d %H:%M:%S",
    )
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
