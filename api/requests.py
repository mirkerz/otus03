from . import fields
from .exceptions import ValidationError
from .constants import ADMIN_LOGIN


class RequestMeta(type):
    """ Request meta class used to store all fields into one list
    """

    def __init__(cls, name, bases, dct):
        super().__init__(name, bases, dct)
        # Combine interested fields into one and tidy dictionary
        cls.fields = {
            attr: value
            for attr, value in dct.items()
            if isinstance(value, fields.Field)
        }


class Request(metaclass=RequestMeta):
    """
    Обработчик принятых структур. Валидирует входные данные, преобразует их
    если необходимо, выставляет при этом необходимый контекс и предоставляет
    эндпойт для генерации значений для ответа

    Схема описывается через задание аттрибутов класса Field. В метаклассе они
    собираются в аттрибут `fields`.  При иницализации входные данные
    сохраняются в аттрибут `data`, они валидируются методом `clean`. Контекст,
    полученный после валидации доступен через метод `get_context`
    """

    def __init__(self, data):
        self.error_messages = {}
        self.data = data
        self.clean_data = {}
        self.context = {}

    def clean(self):
        for name, field in self.fields.items():
            if name not in self.data:
                if field.required:
                    self.error_messages.update({name: "field is required"})
                self.clean_data[name] = field.empty_value
                continue
            try:
                self.clean_data[name] = field.clean(self.data[name])
            except ValidationError as error:
                self.error_messages.update({name: str(error)})

    def is_valid(self):
        if not hasattr(self, "_clean"):
            self.clean()
            if self.error_messages:
                self._clean = False
            else:
                self._clean = True
        return self._clean


class ClientsInterestsRequest(Request):
    client_ids = fields.ClientIDsField(required=True)
    date = fields.DateField(required=False, nullable=True)

    def get_context(self):
        if "client_ids" not in self.clean_data:
            return {}
        return {"nclients": len(self.clean_data["client_ids"])}


class OnlineScoreRequest(Request):
    """
    Аргументы
    + phone ‑ строка или число, длиной 11, начинается с 7, опционально, может
    быть пустым
    + email ‑ строка, в которой есть @, опционально, может быть пустым
    + first_name ‑ строка, опционально, может быть пустым
    + last_name ‑ строка, опционально, может быть пустым
    + birthday ‑ дата в формате DD.MM. YYYY, с которой прошло не больше 70 лет,
    опционально, может быть пустым
    + gender ‑ число 0, 1 или 2, опционально, может быть пустым

    Валидация аругементов аргументы валидны, если валидны все поля по
    отдельности и если присутсвует хоть одна пара phone‑email, first name‑last
    name, gender‑birthday с непустыми значениями.  Контекст в словарь контекста
    должна прописываться запись "has" ‑ список полей, которые были не пустые
    для данного запроса

    Ответ в ответ выдается число, полученное вызовом функции get_score (см.
    scoring.py).
    """

    first_name = fields.CharField(required=False, nullable=True)
    last_name = fields.CharField(required=False, nullable=True)
    email = fields.EmailField(required=False, nullable=True)
    phone = fields.PhoneField(required=False, nullable=True)
    birthday = fields.BirthDayField(required=False, nullable=True)
    gender = fields.GenderField(required=False, nullable=True)

    pairs = (
        ("phone", "email"),
        ("first_name", "last_name"),
        ("gender", "birthday"),
    )

    def field_filled(self, field):
        """ Check if given field is not empty i.e. it's present in clean data
        and it's value isn't considered to be empty """
        if field not in self.clean_data:
            return False
        return self.clean_data[field] not in self.fields[field].empty_values

    def check_pair(self, pair):
        return self.field_filled(pair[0]) and self.field_filled(pair[1])

    def clean(self):
        super().clean()
        if self.error_messages:
            return
        if not any((self.check_pair(pair) for pair in self.pairs)):
            self.error_messages.update({"fields": "no data for scoring"})

    def get_context(self):
        has = []
        for field in self.fields:
            value = self.clean_data.get(field, None)
            if value not in self.fields[field].empty_values:
                has.append(field)
        return {"has": has}


class MethodRequest(Request):
    account = fields.CharField(required=False, nullable=True)
    login = fields.CharField(required=True, nullable=True)
    token = fields.CharField(required=True, nullable=True)
    arguments = fields.ArgumentsField(required=True, nullable=True)
    METHODS = {
        "online_score": OnlineScoreRequest,
        "clients_interests": ClientsInterestsRequest,
    }
    method = fields.ChoicesField(
        choices=METHODS, required=True, nullable=False
    )

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN
