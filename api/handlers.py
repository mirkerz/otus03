"""
Handlers for requests
"""
from .scoring import get_interests, get_score


def online_score_handler(*, store, is_admin, data):
    if is_admin:
        return {"score": 42}
    score = get_score(store, **data)
    return {"score": score}


def clients_interests_handler(*, store, is_admin, data):
    interests = {}
    for client in data["client_ids"]:
        interests[client] = get_interests(store, client)
    return interests


HANDLERS = {
    'online_score': online_score_handler,
    'clients_interests': clients_interests_handler
}
